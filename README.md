# Recording Strix aluco
Research question: what is the best time of year and time of day -or night- to record tawny owls (*Strix aluco*)?

## Analysis and results
The results and the scripts can be [viewed online](https://gitlab.com/alvarosaurus/recording-strix-aluco/blob/master/Recording%20Strix%20aluco.ipynb) as a Jupyter notebook.

## Reproduce
This study and all its dependencies are provided as a Docker container. You will need to install Docker on your machine to run it.
* get Docker at: https://www.docker.com/get-started
* get `docker-compose`at: https://docs.docker.com/compose/install/
then do:
1. install the Jupyter notebook with the bioacoustic analysis software: `./install`
2. launch the Jupyter notebook: `./start`
3. open the given address in a browser
4. open the notebook file `Recording Strix aluco.ipynb`


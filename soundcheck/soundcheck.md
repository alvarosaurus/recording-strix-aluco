# Unsupervised recording
Used 5.5kHz white noise for testing.

Recorder without bag (01_none.png):
* response reasonably flat until 5kHz
* decays until 22kHz

Recorder inside plastic bag (02_bag.png):
* response reasonably flat until 5kHz
* decays until 15kHz
* 6dB lower
* within the bag, noisy
